FROM alpine:3.7
RUN mkdir /app
ADD . /app/
WORKDIR /app
ARG env
ARG config
ARG version
ENV version=${version}
ENV env=${env}
ENV config=${config}
CMD ["/app/app-exe"]
